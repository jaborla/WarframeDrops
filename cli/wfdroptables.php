<?php
spl_autoload_register(function($class) {include("../core/" . $class . ".php");});
$options = getopt(
    "ipca",
    array(
        "item:",
        "planet:",
        "category:",
        "all"
    ));
$cli = new WarframeDropTables_CLI();
$cli->run($options);