<?php

/**
 * Class to perform the necessary rendering for HTML output
 */
class WarframeDropTables_HTMLRenderer extends WarframeDropTables_Renderer
{

    /**
     * Render all the data using HTML tables
     * 
     * @param $args
     */
    protected function render_table($args = array())
    {
        ?>
        <H2>All Mission Drops</H2>
        <table id="all-data">
        <?php
        foreach($this->data as $category => $rotations)
        {
            ?>
            <tr>
                <td colspan="5" class="category-label"><?php echo $this->Reader->translateCategory($category) ?></td>
            </tr>
                <?php
                foreach($rotations as $rotation => $values)
                {
                    if("Levels" == $rotation)
                    {
                ?>
                        <tr>
                            <td colspan="5" class="levels-label">Levels</td>
                        </tr>
                        <?php
                        foreach($values as $level)
                        {
                            ?>
                            <tr>
                                <td class="planet" colspan="2"><?php echo $level['planet'] ?></td>
                                <td class="level"><?php echo $level['level'] ?></td>
                                <td class="type"><?php echo $this->Reader->translateLevel($level['type']) ?></td>
                                <td class="faction"><?php echo $this->Reader->translateFaction($level['faction']) ?></td>
                            </tr>
                            <?php
                        }
                    }
                    else // Items
                    {
                        ?>
                        <tr>
                            <td colspan="5" class="rotation-label"><?php echo $rotation ?></td>
                        </tr>
                        <?php
                        foreach ($values as $value)
                        {
                        ?>
                        <tr>
                            <td class="quantity"><?php echo $value['quantity'] ?></td>
                            <td class="item"><?php echo $value['name'] ?></td>
                            <td class="rate"><?php echo $value['rate'] ?></td>
                            <td class="rarity"><?php echo $value['rarity'] ?></td>
                            <td class="note"><?php if(isset($value['note'])) echo $value['note'] ?></td>
                        </tr>
                        <?php
                        }

                    }
                }}
    }

    protected function render_css3($args = array())
    {
        ?>
        <div id="all-data-table">
        <?php
        if(empty($this->data))
        {
            ?>
            <div class="not-found">
                <p>No results found.</p>
            </div>
            <?php
            return false;
        }
        foreach($this->data as $category => $rotations)
        {
            ?>
            <div class="category-wrapper" id="<?php echo $category ?>-wrapper">
                <div class="category-label" id="<?php echo $category ?>-label">
                    <?php echo $this->Reader->translateCategory($category) ?>
                </div>
                <div class="category-children" id="<?php echo $category ?>-children">
                    <?php
                    foreach($rotations as $rotation => $values)
                    {
                        if("Levels" == $rotation)
                        {
                    ?>
                            <div class="levels-wrapper">
                                <div class="levels-label">
                                    Levels
                                </div>
                                <div class="levels-header-row levels-row">
                                    <div class="planet-header">
                                        Planet
                                    </div>
                                    <div class="level-header">
                                        Level Name
                                    </div>
                                    <div class="type-header">
                                        Mission Type
                                    </div>
                                    <div class="faction-header">
                                        Opposing Faction
                                    </div>
                                </div>
                            <?php
                            foreach($values as $level)
                            {
                                ?>
                                <div class="levels-row">
                                    <div class="planet"><?php echo $level['planet'] ?></div>
                                    <div class="level"><?php echo $level['level'] ?></div>
                                    <div class="type"><?php echo $this->Reader->translateLevel($level['type']) ?></div>
                                    <div class="faction"><?php echo $this->Reader->translateFaction($level['faction']) ?></div>
                                </div>
                                <?php
                            }
                            ?>
                            </div>
                            <?php
                        }
                        else // Items
                        {
                            ?>
                            <div class="rotation-wrapper">
                                <div class="rotation-label">
                                    <?php echo $rotation ?>
                                </div>
                            <?php
                            foreach ($values as $value)
                            {
                            ?>
                                <div class="item-row">
                                    <div class="quantity"><?php echo $value['quantity'] ?></div>
                                    <div class="item"><?php echo $value['name'] ?></div>
                                    <div class="rate"><?php echo $value['rate'] ?></div>
                                    <div class="rarity"><?php echo $value['rarity'] ?></div>
                                    <div class="note"><?php if(isset($value['note'])) echo $value['note']; else echo "&nbsp;"?></div>
                                </div>
                            <?php
                            }
                            ?>
                            </div>
                            <?php
                        }
                    }
                ?>
                </div>&nbsp;
            </div>
            <?php
        }
        ?>
        </div>
        <?php
        return true;
    }

    protected function render_search($args)
    {
        if(!isset($args['query']) || "" == trim($args['query']))
        {
            return $this->render_css3();
        }
        $this->setData($this->Reader->searchForSetOrItem($args['query']));
        $this->render_css3();
        ?>
        <script type="text/javascript">
            jQuery('#search-input').attr('value','<?php echo $args['query'] ?>');
            jQuery('.search').show();
            jQuery(document).ready(function() {
            setTimeout(function() {
                toggleAll('expand-all');
            }, 10);
            });
        </script>
        <?php

        return true;
    }

    protected function render_planet_search($args)
    {
        if(!isset($args['query']) || "" == trim($args['query']))
            {
                return $this->render_css3();
            }
            $this->setData($this->Reader->listLevelsForPlanet(strtolower($args['query'])));
            if(empty($this->data))
            {
                ?>
                <div class="not-found">
                    <p>No results found.</p>
                </div>
                <?php
                $return = false;
            }
            else
            {
            ?>
            <div class="levels-wrapper">
                <div class="levels-label"><?php echo ucfirst($args['query']); ?></div>
                <div class="levels-header-row levels-row">
                    <div class="level-header">
                        Level Name
                    </div>
                    <div class="category-header">
                        Category
                    </div>
                    <div class="type-header">
                        Mission Type
                    </div>
                    <div class="faction-header">
                        Opposing Faction
                    </div>
                </div>
                <?php
                foreach ($this->data as $level)
                {
                    ?>
                    <div class="levels-row">
                        <div class="level">
                            <?php echo ucfirst($level['level']); ?>
                        </div>
                        <div class="level-category">
                            <?php echo ucfirst($this->Reader->translateCategory($level['category'])); ?>
                        </div>
                        <div class="type">
                            <?php echo ucfirst($this->Reader->translateLevel($level['type'])); ?>
                        </div>
                        <div class="faction">
                            <?php echo ucfirst($this->Reader->translateFaction($level['faction'])); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <?php
                $return = true;
            }
            ?>
            <script type="text/javascript">
                jQuery('#planet-input').attr('value','<?php echo $args['query'] ?>');
                jQuery('.planet-levels').show();
                jQuery(document).ready(function() {
                setTimeout(function() {
                    toggleAll('expand-all');
                }, 10);
                });
            </script>
            <?php

            return $return;
    }

    protected function render_category_search($args)
    {
        if(!isset($args['query']) || "" == trim($args['query']))
        {
            return $this->render_css3();
        }
        $this->setData($this->Reader->searchForCategory(strtolower($args['query'])));
        if(empty($this->data))
        {
            ?>
            <div class="not-found">
                <p>No results found.</p>
            </div>
            <?php
            return false;
        }
        else
        {
            return $this->render_css3();
        }
    }

}