<?php

/**
 * Class for rendering output to the command line
 */
class WarframeDropTables_CLIRenderer extends WarframeDropTables_Renderer
{

    /**
     * Take the array of processed data and display it on the screen
     *
     * @param $args
     */
    public function render_all($args = array())
    {
        foreach($this->data as $category => $rotations)
        {
            echo "\n" . $this->Reader->translateCategory($category) . "\n-----\n";
            foreach($rotations as $rotation => $items)
            {
                if("Levels" == $rotation)
                {
                    echo 'Levels which apply: ' . "\n";
                    foreach($items as $level)
                    {
                        echo "\t" . $level['level'] . " on " . $level['planet']. ": " .
                            $this->Reader->translateLevel($level['type']) . " against " .
                            $this->Reader->translateFaction($level['faction']) . "\n";
                    }
                }
                else // Items
                {
                    echo $rotation . ":\n";
                    foreach($items as $item)
                    {
                        echo "\t" . $item['quantity'] . "x " . $item['name'] . ":\t" .
                            "Rate: " . $item['rate'] . "\t" .
                            "Rarity: " . $item['rarity'] . "\t" .
                            (isset($item['note']) ? "Note: " . $item['note'] : "") . "\n";

                    }
                }
            }
        }
    }

    /**
     * Render only a search result for one item
     *
     * @param array $args
     */
    public function render_item($args = array())
    {
        if(isset($args['query']) || "" != trim($args['query']))
            $this->setData($this->Reader->searchForSetOrItem(strtolower($args['query'])));
        if(empty($this->data))
            echo "No results found for Item named '" . $args['query'] . "'";
        return $this->render_all();
    }

    /**
     * Render search result for categories by name
     *
     * @param array $args
     */
    public function render_category($args = array())
    {
        if(isset($args['query']) || "" != trim($args['query']))
            $this->setData($this->Reader->searchForCategory(strtolower($args['query'])));
        if(empty($this->data))
            echo "No results found for Category '" . $args['query'] . "'";
        else
            return $this->render_all();
    }

    /**
     * List levels for a planet
     *
     * @param array $args
     */
    public function render_planet($args = array())
    {
        if(!isset($args['query']) || "" == trim($args['query']))
        {
            echo "No planet found with name '" . $args['quer'] . "'";
            return false;
        }
        $this->setData($this->Reader->listLevelsForPlanet(strtolower($args['query'])));
        echo "Levels found for Planet '" . $args['query'] . "'" .
            "\n-----\n";
        foreach ($this->data as $level)
        {
                echo ucfirst($level['level']) . ":\t" .
                ucfirst($this->Reader->translateCategory($level['category'])) . "\t|\t" .
                ucfirst($this->Reader->translateLevel($level['type'])) . " against " .
                ucfirst($this->Reader->translateFaction($level['faction'])) . "\n";
        }
    }

}