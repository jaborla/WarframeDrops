<?php
/**
 * class WarframeDropTables_Core
 *
 * Read the file from github, translate into
 */

class WarframeDropTables_Core
{
    protected $source_url;
    protected $category_dictionary;
    protected $level_dictionary;
    protected $faction_dictionary;
    protected $process;
    protected $local_backup;
    protected $age;
    protected $full_data;
    protected $planet_level_data;
    protected $renderer;
    
    const DEBUG_MODE = true;

    public function __construct()
    {
        $this->source_url   = 'https://raw.githubusercontent.com/VoiDGlitch/WarframeData/master/MissionDecks.txt';
        $this->local_backup = 'data.txt';
        $this->process      = 'display';
        $this->age          = 1;
    }

    public function setProcess($process = "display")
    {
        $this->process = $process;
    }

    public function setFullData($new_array = array())
    {
        $this->full_data = $new_array;
    }

    public function setLocalBackup($new_filename = "localbackup.txt")
    {
        $this->local_backup = $new_filename;
    }

    /**
     * Set the rendering type to use
     *
     * @param $new_mode
     */
    public function setMode($new_mode)
    {
        switch ($new_mode)
        {
            case "html":
                $this->renderer = new WarframeDropTables_HTMLRenderer($this);
                break;
            case "cli":
                $this->renderer = new WarframeDropTables_CLIRenderer($this);
                break;
            default:
                break;
        }
    }

    /**
     * Actually render the results
     * 
     * @param $args
     */
    public function render($args)
    {
        if(empty($this->full_data))
            $this->load();
        $this->renderer->render($this->full_data, $args);
    }

    /**
     * Wrapper function to call methods of processing
     *
     * @param bool $override
     */
    public function run($override = false, $args = false)
    {
        if($override)
            $this->setProcess($override);

        if(empty($this->full_data))
            $this->load();

        return $this->{$this->process}($args);
    }

    /**
     * Load the local file backup into memory
     *
     * @param bool $override
     */
    public function load($override = false)
    {
        //TODO: Sort the data in a reasonable order.  I'm thinking Missions, Towers, Quests, Events, PvP, Misc.  At least get the order right so it doesn't go 3, 1, 2.
        if($override)
            $this->setLocalBackup($override);
        echo "";
        if(false === file_exists($this->local_backup) ||
            (time() - filemtime($this->local_backup) > ($this->age * 24 * 3600)))
        {
            $this->save();
        }
        $text_stream = "";
        $file_handle = fopen($this->local_backup, "r");
        if (false === $file_handle)
            $this->error("Failed to read local file.");
        else
        {
            while (!feof($file_handle))
                $text_stream .= fread($file_handle, 2048);
        }
        $this->full_data = unserialize($text_stream);
        unset($text_stream);

        if(false === file_exists("levels" . $this->local_backup) ||
            (time() - filemtime("levels" . $this->local_backup) > ($this->age * 24 * 3600)))
        {
            $this->save();
        }
        $text_stream = "";
        $file_handle = fopen("levels" . $this->local_backup, "r");
        if (false === $file_handle)
            $this->error("Failed to read local file.");
        else
        {
            while (!feof($file_handle))
                $text_stream .= fread($file_handle, 2048);
        }
        $this->planet_level_data = unserialize($text_stream);
        unset($text_stream);
    }

    /**
     * Get the remote URL and save it locally
     *
     * @param bool $override
     * @return bool
     */
    public function save($override = false)
    {
        if($override)
        {
            $this->setLocalBackup($override);
        }
        $text_stream = "";
        $file_handle = fopen($this->source_url,"r");
        if(false === $file_handle)
            $this->error("Failed to read source url.");
        else
        {
            while (!feof($file_handle))
                $text_stream .= fread($file_handle, 2048);
        }
        $stream_array = explode("\n",$text_stream);
        unset($text_stream);

        $final_array = array();
        $current_category = "";
        $current_rotation = "";
        foreach($stream_array as $line)
        {
            if ("" == trim($line))
                continue;
            if(preg_match('/\[.*\]/',$line))
            {
                $current_category = trim($line,"[]");
                $this->debug("new category: $current_category\n");
            }
            else
            {
                if("" === $current_category)
                    continue;
                elseif(ltrim($line)==$line)
                {
                    // The rotation
                    $current_rotation = trim($line,":");
                    $this->debug("new rotation: $current_rotation\n");
                }
                elseif(0===strpos(ltrim($line),"-"))
                {
                    // The levels for the category
                    $trimmed_array = explode(",",trim($line,"\t\n\r\0- "));
                    $levels = array(
                        "planet" => $trimmed_array[0],
                        "level" => $trimmed_array[1],
                        "type" => $trimmed_array[2],
                        "faction" => $trimmed_array[3]
                    );
                    $final_array[$current_category]["Levels"][] = $levels;
                }
                else
                {
                    if("" === $current_rotation)
                        continue;
                    $details = explode(",",trim($line));
                    if(false !== strpos($details[0],":"))
                    {
                        // Random stuff like Attenuation
                        $this->debug("Random variable found.  Removing: " . $details[0]);
                        continue;
                    }
                    $item = array(
                        "name" => substr($details[0],strpos($details[0]," ")),
                        "quantity" => substr($details[0],0,strpos($details[0]," ")),
                        "rarity" => $details[1],
                        "rate" => $details[2],
                    );
                    if(isset($details[3]))
                        $item["note"] = $details[3];
                    $item = array_map('trim',$item);
                    $final_array[$current_category][$current_rotation][] = $item;
                }
            }
        }
        unset($stream_array);
        $local_backup = fopen($this->local_backup,"w");
        fwrite($local_backup,serialize($final_array));
        fclose($local_backup);
        $this->full_data = $final_array;

        $this->gatherPlanetLevelList();
        $local_backup2 = fopen("levels" . $this->local_backup,"w");
        fwrite($local_backup2,serialize($this->planet_level_data));
        fclose($local_backup2);
        return true;
    }

    /**
     * Make an array of the planet/level list
     *
     * @return bool
     */
    protected function gatherPlanetLevelList()
    {
        if(empty($this->full_data))
            $this->load();
        $found = array();
        foreach($this->full_data as $category => $rotations)
        {
            if (!isset($rotations['Levels']))
                continue;

            foreach($rotations['Levels'] as $level_array)
            {
                if(!isset($found[trim(strtolower($level_array['planet']))])
                    || !in_array(trim(strtolower($level_array['level'])),$found[trim(strtolower($level_array['planet']))] ))
                    $found[trim(strtolower($level_array['planet']))][] = array(
                        "level" => trim(strtolower($level_array['level'])),
                        "category" => $category,
                        "type" => $level_array['type'],
                        "faction" => $level_array['faction']
                    );
            }
        }

        if(!empty($found))
        {
            $this->planet_level_data = $found;

            return true;
        }
        else
        {
            $this->error("Unable to find any levels in the data.");
            return false;
        }
    }

    /**
     * Translates a particular category of drops into its human-legible counterpart
     *
     * @param $key
     * @return mixed
     */
    public function translateCategory($key)
    {
        $key = trim($key);
        if(!isset($this->category_dictionary))
        {
            $this->setDefaultCategoryDictionary();
        }
        if(isset($this->category_dictionary[$key]))
            return $this->category_dictionary[$key];
        else
            return $key;
    }

    /**
     * Translate the level type names into human-readable equivalent
     *
     * @param $key
     * @return mixed
     */
    public function translateLevel($key)
    {
        $key = trim($key);
        if(!isset($this->level_dictionary))
        {
            $this->setDefaultLevelDictionary();
        }
        if(isset($this->level_dictionary[$key]))
            return $this->level_dictionary[$key];
        else
            return $key;
    }

    /**
     * Translate faction code into human-readable
     *
     * @param $key
     * @return string
     */
    public function translateFaction($key)
    {
        $key = trim($key);
        if(!isset($this->faction_dictionary))
        {
            $this->setDefaultFactionDictionary();
        }
        if(isset($this->faction_dictionary[$key]))
            return $this->faction_dictionary[$key];
        else
            return $key;
    }

    /**
     * Sets the default category translation table
     */
    protected function setDefaultCategoryDictionary()
    {
        $this->category_dictionary = array(
            "ArchwingCacheCoordsReward" => "Archwing Quest Sabotage",
            "ArchwingExterminateRewards" => "Archwing Exterminate",
            "ArchwingMobileDRewards" => "Archwing Mobile Defense",
            "ArchwingSabotageRewards" => "Archwing Sabotage",
            "BossMissionRewards/AtlasRewards" => "Jordas Golem Assassination",
            "BossMissionRewards/BerserkerRewards" => "Alad V Assassination",
            "BossMissionRewards/CowgirlRewards" => "Mutalist Alad V Assassination",
            "BossMissionRewards/EmberRewards" => "Sargas Ruk Assassination",
            "BossMissionRewards/ExcaliburRewards" => "Ambulas Assassination",
            "BossMissionRewards/FrostRewards" => "Lech Kril Assassination",
            "BossMissionRewards/HydroidRewards" => "Vay Hek Assassination",
            "BossMissionRewards/LokiRewards" => "Hyena Pack Assassination",
            "BossMissionRewards/MagRewards" => "The Sergeant Assassination",
            "BossMissionRewards/MiterGremlinsRewards" => "Kril and Vor Assassination",
            "BossMissionRewards/NovaRewards" => "Raptor Assassination",
            "BossMissionRewards/NyxRewards" => "Phorid Assassination",
            "BossMissionRewards/RhinoRewards" => "Jackal Assassination",
            "BossMissionRewards/SarynRewards" => "Kela De Thaym Assassination",
            "BossMissionRewards/SeerPistolRewards" => "Vor Assassination",
            "BossMissionRewards/TrinityRewards" => "Kril and Vor Assassination: Trinity",
            "BossMissionRewards/YinYangRewards" => "Tyl Regor Assassination",
            "CacheRewardsEasy" => "Sabotage Tier 1",
            "CacheRewardsHard" => "Sabotage Tier 3",
            "CacheRewardsMedium" => "Sabotage Tier 2",
            "CaptureMissionRewardsA" => "Capture Tier 1",
            "ClemCloneRewards" => "Weekly Clem Mission",
            "CorpusInfestedSabotageRewards" => "Hive Sabotage",
            "DefenseMissionRewards/ArchwingDefenceMissionRewardsEasy" => "Archwing Defense Tier 1",
            "DefenseMissionRewards/BadlandsDefenseMissionRewards" => "Dark Sector Defense",
            "DefenseMissionRewards/DefenseMissionRewardsEasy" => "Defense Tier 1",
            "DefenseMissionRewards/DefenseMissionRewardsHard" => "Defense Tier 3",
            "DefenseMissionRewards/DefenseMissionRewardsMedium" => "Defense Tier 2",
            "DerelictDefenseRewards" => "Derelict Defense",
            "DerelictSurvivalRewards" => "Derelict Survival",
            "ExtractionMissionRewards/ExtractionLowLevelRewards" => "Excavation Tier 1",
            "ExtractionMissionRewards/LimboChassisQuestExcavationRewards" => "Limbo Quest Chassis",
            "ExtractionMissionRewards/LimboHelmetQuestExcavationRewards" => "Limbo Quest Helmet",
            "ExtractionMissionRewards/LimboSystemsQuestExcavationRewards" => "Limbo Quest Systems",
            "ForestSabotageRewards" => "Grineer Forest Sabotage",
            "FusionCoreRewards" => "Fusion Cores",
            "GolemMissionRewards" => "Derelict Assassination",
            "HalloweenExtermEventRewards" => "Halloween Exterminate Event",
            "InfestedIncursionsSurvivalRewards" => "",
            "InterceptionMissionRewards/ArchwingInterceptionMissionRewardsHard" => "Archwing Interception Tier 3",
            "InterceptionMissionRewards/InterceptionMissionRewardsEasy" => "Interception Tier 1",
            "InterceptionMissionRewards/InterceptionMissionRewardsHard" => "Interception Tier 3",
            "InterceptionMissionRewards/InterceptionMissionRewardsMedium" => "Interception Tier 2",
            "InterceptRewardsGrineer" => "Grineer Interception",
            "KelaEventRewardsEasy" => "",
            "KelaEventRewardsHard" => "",
            "KelaEventRewardsMedium" => "",
            "MacheteMayhemBonusRewards" => "",
            "MirageRewards" => "Mirage Quest Rewards",
            "MissionRewardsCorpus" => "",
            "MissionRewardsGrineer" => "",
            "MissionRewardsInfested" => "",
            "NightmareModeRewards" => "Nightmare Rewards",
            "OceanSabotageRewards" => "Uranus Archwing Sabotage",
            "OrokinDerelictSabotageRewards" => "Derelict Sabotage",
            "OrokinSabotageEventRewards" => "",
            "ProjectFriendlyFireRewardsBasic" => "",
            "ProjectFriendlyFireRewardsBonus" => "",
            "ProjectPanaceaBonusRewards" => "",
            "ProjectSinisterRewardsBasic" => "",
            "ProjectSinisterRewardsBonus" => "",
            "ProjectSinisterRewardsFinal" => "",
            "ProjectUndermineBonusRewards" => "",
            "ProjectUndermineRewards" => "",
            "PVPMissionRewards/CTFRewards" => "",
            "PVPMissionRewards/DMRewards" => "",
            "PVPMissionRewards/TDMRewards" => "",
            "RaidRewards/GolemRaid" => "",
            "RaidRewards/HekRaid" => "",
            "RaidRewards/NightmareHekRaid" => "",
            "RaidRewards/NightmareResourceHekRaid" => "",
            "RescueMissionRewards/RescueMissionRewardsEasy" => "Rescue Tier 1",
            "RescueMissionRewards/RescueMissionRewardsHard" => "Rescue Tier 3",
            "RescueMissionRewards/RescueMissionRewardsMedium" => "Rescue Tier 2",
            "RescueMissionRewards/RescueMissionRewardsNightmare" => "Nightmare Rescue Specters",
            "SortieRewards" => "Sortie Season 1",
            "SortieRewardsB" => "Sortie Season 2",
            "SortieRewardsC" => "Sortie Season 3",
            "SortieRewardsD" => "Sortie Season 4",
            "SortieRewardsE" => "Sortie Season 5",
            "SortieRewardsF" => "Sortie Season 6",
            "SpyMissionRewards/SpyHighMissionRewards" => "Spy Tier 3",
            "SpyMissionRewards/SpyLowMissionRewards" => "Spy Tier 1",
            "SpyMissionRewards/SpyMediumMissionRewards" => "Spy Tier 2",
            "SpyMissionRewards/SpyMoonMissionRewards" => "Orokin Moon Spy",
            "SurvivalMissionRewards/SurvivalBadlandsHighLevelRewards" => "Dark Sector Survival Tier 3",
            "SurvivalMissionRewards/SurvivalBadlandsLowLevelRewards" => "Dark Sector Survival Tier 1",
            "SurvivalMissionRewards/SurvivalBadlandsMidLevelRewards" => "Dark Sector Survival Tier 2",
            "SurvivalMissionRewards/SurvivalBadlandsUltraLevelRewards" => "Dark Sector Survival Tier 4",
            "SurvivalMissionRewards/SurvivalGeneticFoundryRewards" => "",
            "SurvivalMissionRewards/SurvivalHighLevelRewards" => "Survival/Excavation Tier 3",
            "SurvivalMissionRewards/SurvivalLowLevelRewards" => "Survival/Excavation Tier 1",
            "SurvivalMissionRewards/SurvivalMidLevelRewards" => "Survival/Excavation Tier 2",
            "VaultRewards" => "Derelict Vault Rewards",
            "VoidKeyMissionRewards/OrokinCaptureRewardsA" => "Tower I Capture",
            "VoidKeyMissionRewards/OrokinCaptureRewardsB" => "Tower II Capture",
            "VoidKeyMissionRewards/OrokinCaptureRewardsC" => "Tower III Capture",
            "VoidKeyMissionRewards/OrokinCaptureRewardsD" => "Tower IV Capture",
            "VoidKeyMissionRewards/OrokinDefenseRewardsA" => "Tower I Defense",
            "VoidKeyMissionRewards/OrokinDefenseRewardsB" => "Tower II Defense",
            "VoidKeyMissionRewards/OrokinDefenseRewardsC" => "Tower III Defense",
            "VoidKeyMissionRewards/OrokinDefenseRewardsD" => "Tower IV Defense",
            "VoidKeyMissionRewards/OrokinExterminateRewardsA" => "Tower I Exterminate",
            "VoidKeyMissionRewards/OrokinExterminateRewardsB" => "Tower II Exterminate",
            "VoidKeyMissionRewards/OrokinExterminateRewardsC" => "Tower III Exterminate",
            "VoidKeyMissionRewards/OrokinExterminateRewardsD" => "Tower IV Exterminate",
            "VoidKeyMissionRewards/OrokinInterceptionRewardsD" => "Tower IV Interception",
            "VoidKeyMissionRewards/OrokinMobileDefenseRewardsA" => "Tower I Mobile Defense",
            "VoidKeyMissionRewards/OrokinMobileDefenseRewardsB" => "Tower II Mobile Defense",
            "VoidKeyMissionRewards/OrokinMobileDefenseRewardsC" => "Tower III Mobile Defense",
            "VoidKeyMissionRewards/OrokinMobileDefenseRewardsD" => "Tower IV Mobile Defense",
            "VoidKeyMissionRewards/OrokinSabotageRewardsA" => "Tower I Sabotage",
            "VoidKeyMissionRewards/OrokinSabotageRewardsB" => "Tower II Sabotage",
            "VoidKeyMissionRewards/OrokinSabotageRewardsC" => "Tower III Sabotage",
            "VoidKeyMissionRewards/OrokinSabotageRewardsD" => "Tower IV Sabotage",
            "VoidKeyMissionRewards/OrokinSurvivalRewardsA" => "Tower I Survival",
            "VoidKeyMissionRewards/OrokinSurvivalRewardsB" => "Tower II Survival",
            "VoidKeyMissionRewards/OrokinSurvivalRewardsC" => "Tower III Survival",
            "VoidKeyMissionRewards/OrokinSurvivalRewardsD" => "Tower IV Survival",
        );

        // Delete any entries we've left blank.
        $this->category_dictionary = array_filter($this->category_dictionary);
    }

    /**
     * Sets the translate table for mission types
     */
    protected function setDefaultLevelDictionary()
    {
        $this->level_dictionary = array(
            "MT_SURVIVAL" => "Survival",
            "MT_EXCAVATE" => "Excavation",
            "MT_EXTERMINATION" => "Exterminate",
            "MT_MOBILE_DEFENSE" => "Mobile Defense",
            "MT_SABOTAGE" => "Sabotage",
            "MT_ASSASSINATION" => "Assassination",
            "MT_CAPTURE" => "Capture",
            "MT_HIVE" => "Hive Sabotage",
            "MT_DEFENSE" => "Defense",
            "MT_TERRITORY" => "Interception",
            "MT_ARENA" => "Arena",
            "MT_PVP" => "Player vs Player",
            "MT_RESCUE" => "Rescue",
            "MT_INTEL" => "Spy"
        );

        $this->level_dictionary = array_filter($this->level_dictionary);
    }

    /**
     * Set the default faction translation dictionary
     */
    protected function setDefaultFactionDictionary()
    {
        $this->faction_dictionary = array(
            "FC_INFESTATION" => "Infested",
            "FC_GRINEER" => "Grineer",
            "FC_CORPUS" => "Corpus",
        );
        
        $this->faction_dictionary = array_filter($this->faction_dictionary);
    }

    /**
     * Find all instances of a particular item
     *
     * @param $item_name
     * @return mixed
     */
    protected function searchForItem($item_name)
    {
        if(empty($this->full_data))
            $this->load();
        $found = array();
        foreach($this->full_data as $category => $rotations)
        {
            foreach($rotations as $rotation => $values)
            {
                foreach($values as $index => $value)
                {
                    if(isset($value['name']) && (false !== strpos(strtolower($value['name']),strtolower($item_name))))
                    {
                        if(isset($this->full_data[$category]['Levels']))
                            $found[$category]['Levels'] = $this->full_data[$category]['Levels'];
                        $found[$category][$rotation][] = $values[$index];
                    }
                }
            }
        }
        if(!empty($found))
            return $found;

        return false;
    }

    /**
     * Redundant function for set searching; here in case item searching gets less sensitive
     *
     * @param $set_name
     * @return mixed
     */
    protected function searchForSet($set_name)
    {
        return $this->searchForItem($set_name);
    }

    public function searchForSetOrItem($name)
    {
        return $this->searchForItem($name);
    }
    
    /**
     * Gets the subset of information for a particular category
     *
     * @param $category_name
     * @return mixed
     */
    public function searchForCategory($category_name)
    {
        if(empty($this->full_data))
            $this->load();
        $found = array();
        foreach($this->full_data as $category => $values)
        {
            if(false !== strpos(strtolower($this->translateCategory($category)),strtolower($category_name)))
                $found[$category] = $values;
        }
        if(!empty($found))
            return ($found);
        else
            return false;

    }

    /**
     * Find the planet a certain level is on
     *
     * @param $level_name
     * @return array|bool
     */
    protected function searchForLevel($level_name)
    {
        if(empty($this->planet_level_data))
            $this->gatherPlanetLevelList();
        $found = array();
        foreach($this->planet_level_data as $planet => $levels)
        {
            foreach($levels as $level)
            {
                if(false !== strpos(strtolower($level),strtolower($level_name)))
                    $found[] = $planet;
            }
        }

        if(!empty($found))
            return $found;

        return false;
    }

    /**
     * Get all the levels for a particular category
     *
     * @param $category_name
     * @return array|bool
     */
    protected function listLevels($category_name)
    {
        if(empty($this->full_data))
            $this->load();
        if(!isset($this->full_data[$category_name]) || !isset($this->full_data[$category_name]["Levels"]))
            return false;
        else
        {
            $found = array();
            foreach($this->full_data[$category_name]["Levels"] as $level)
            {
                $found[] = $level;
            }

            return $found;
        }
    }

    /**
     * Return all the levels for a given planet
     *
     * @param $planet_name
     * @return array|bool
     */
    public function listLevelsForPlanet($planet_name)    
    {
        if(empty($this->planet_level_data))
            $this->gatherPlanetLevelList();
        if(isset($this->planet_level_data[strtolower($planet_name)]))
            return $this->planet_level_data[strtolower($planet_name)];

        return false;
    }

    /**
     * Output an error and die.
     *
     * @param $message
     */
    protected function error($message)
    {
        if(self::DEBUG_MODE)
            die($message);
        else
            die();
    }

    /**
     * Output a debug message
     *
     * @param $message
     */
    protected function debug($message)
    {
        if(self::DEBUG_MODE && !is_a($this->renderer,"WarframeDropTables_HTMLRenderer"))
            echo $message;
    }
}
