<?php

/**
 */
abstract class WarframeDropTables_Renderer
{
    protected $data;
    protected $Reader;

    public function __construct($Reader)
    {
        $this->Reader = $Reader;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function render($source_data, $args)
    {
        if (is_array($source_data) && !empty($source_data))
            $this->setData($source_data);
        $method = "render_" . $args["method"];
        $this->$method($args);
    }
}