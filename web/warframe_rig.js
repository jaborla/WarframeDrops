/**
 * The stuff to run when the doc is ready
 */
jQuery(document).ready(function() {
    jQuery(".category-children").hide();
    jQuery('#expand-all').click(function(){toggleAll('expand-all');});
});

/**
 * Function we need for the expand/collapse button
 */
var toggleAll = function(current_id) {
    var newText;
    var newId;
    switch (current_id)
    {
        case 'expand-all':
            jQuery('.category-children, .item-row, .levels-row').show();
            newText = 'Collapse All';
            newId = 'collapse-all';
            break;
        case 'collapse-all':
            jQuery('.category-children, .item-row, .levels-row').hide();
            newText = 'Expand All';
            newId = 'expand-all';
    }
    jQuery('#'+current_id).attr('id',newId).text(newText).unbind('click').click(function (){toggleAll(newId)});
    return (newId);
};

/**
 * Individual show/hide toggles for parts
 */
jQuery(".category-label").click(function() {
    jQuery(this).siblings('.category-children').slideToggle(400);
});

jQuery(".levels-label").click(function() {
   jQuery(this).siblings('.levels-row').slideToggle(400);
});

jQuery(".rotation-label").click(function() {
    jQuery(this).siblings('.item-row').slideToggle(400);
});

/**
 * Search areas and inputs
 */
jQuery('#search-input, #planet-input, #category-input').focus(function() {
    var box = jQuery(this);
    box.select();

    box.mouseup(function() {
        box.unbind('mouseup');
        return false;
    });
});

jQuery("#search").click(function() {
    jQuery(".search").show();
});

jQuery('#planet-levels').click(function() {
    jQuery('.planet-levels').show();
});

jQuery('#category-levels').click(function() {
    jQuery('.category-search').show();
});

jQuery('#home').click(function() {
    window.location.href = 'index.php';
});

jQuery('.item, .planet, .level-category').addClass('searchable');

jQuery('.item').click(function () {
    jQuery('#search-input').val(jQuery(this).text());
    jQuery('.search-submit').click();
});

jQuery('.planet').click(function() {
    jQuery('#planet-input').val(jQuery(this).text());
    jQuery('.planet-submit').click();
});

jQuery('.level-category').click(function() {
    jQuery('#category-input').val(jQuery.trim(jQuery(this).text()));
    jQuery('.category-submit').click();
});