<!DOCTYPE html>
<HTML>
<HEAD>
    <META charset="UTF-8">
    <META name="viewport" content="initial-scale=1.0, width=device-width" />
    <TITLE>Warframe Drop Tables</TITLE>
    <LINK rel="stylesheet" href="html_styles-shared.css">
    <?php
    if (file_exists("html_styles-" . $mode))
        $second_stylesheet = "html_styles-" . $mode . ".css";
    else
        $second_stylesheet = "html_styles-css3.css";
    ?>
    <LINK rel="stylesheet" href="<?php echo $second_stylesheet ?>">
</HEAD>
<BODY>
<script type="text/javascript" src="jquery-2.2.3.min.js" ></script>
<header>
    <nav>
        <ul>
            <li><button id="home">Home</button></li>
            <li>
                <form action="index.php" method="post">
                    <input type="hidden" name="mode" value="<?php echo $modes[$mode] ?>">
                <button type="submit"><?php echo ucfirst($modes[$mode]) ?> Mode</button>
                </form>
            </li>
            <li><button id="search">Search for Item/Set</button></li>
            <li><button id="planet-levels">Get Levels for Planet</li>
            <li><button id="category-levels">Get Levels for Category</button></li>
            <li><button id="expand-all">Expand all</button></li>
        </ul>
    </nav>
    <div class="search">
        <form action="index.php" method="post">
            <label for="search-input">Item/Set Search:</label>
        <input name="search" type="text" id="search-input"  maxlength="100">
            <input type="hidden" name="mode" value="search">
            <button class="search-submit" type="submit">Go</button>
        </form>
    </div>
    <div class="planet-levels">
        <form action="index.php" method="post">
            <label for="planet-input">Planet Name:</label>
            <input name="planet_search" type="text" id="planet-input" maxlength="100">
            <input type="hidden" name="mode" value="planet_search">
            <button class="planet-submit" type="submit">Go</button>
        </form>
    </div>
    <div class="category-search">
        <form action="index.php" method="post">
            <label for="category-input">Mission Type:</label>
            <input name="category_search" type="text" id="category-input" maxlength="100">
            <input type="hidden" name="mode" value="category_search">
            <button class="category-submit" type="submit">Go</button>
        </form>
    </div>
</header>