<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
spl_autoload_register(function($class) {include("../core/" . $class . ".php");});
$core = new WarframeDropTables_Core();
$core->setMode("html");
$modes = array(
    "css3" => "table",
    "table" => "css3",
    "search" => "table",
    "planet_search" => "table",
    "category_search" => "table"
);

$mode = "css3"; 
if(isset($_POST) && isset($_POST['mode']))
    $mode = $_POST['mode'];

include("header.php");
?>

<?php
    switch($mode)
    {
        case "table":
            $core->render(array("method" => "table"));
            break;
        case "search":
            $core->render(array("method" => "search",
                "query" => $_POST['search']));
            break;
        case "planet_search":
            $core->render(array("method" => "planet_search",
                "query" => $_POST['planet_search']));
            break;
        case "category_search":
            $core->render(array("method" => "category_search",
                "query" => $_POST['category_search']));
            break;
        case "css3":
        default:
            $core->render(array("method" => "css3"));
    }
    if("table" !== $mode)
    {
        ?><script type="text/javascript" src="warframe_rig.js"></script> <?php
    }
    ?>
    </BODY>
    </HTML>
<?php
