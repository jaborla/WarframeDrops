# WarframeDrops

NOTE: This project is abandoned and was migrated from GitHub as-is.

This is a little project to parse the data found here:

https://github.com/VoiDGlitch/WarframeData

I'm writing it in PHP because it's going to have a web service element.

It's intended to be two elements, a server-based web app and a command line app.  Both will eventually have full searchability features.  I created this mainly for my own use, but it could be helpful to others who play the game, so public repo it is.

Potential ideas for expansion once I'm done:

- XML Renderer
- Responsive Design on the Web App
- RESTful API to pull the data for use as a child of other sites.
- Port to Python or Java for practice with those languages
